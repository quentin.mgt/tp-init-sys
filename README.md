# Tp init Sys


### Cette SAE consiste à mettre en place un environnement de travail pour réaliser les travaux de cours à la maison. 

## Mon instalation.

Voici le materiel que j'ai utilisé pour produire mon instalation :

 - [ ] Ordinateur portable avec Windows10 64 bits (HP Probook G8)
 - [ ] Clef usb avec l'ISO d'Ubuntu (Ubuntu 20.04.3 LTS 64 bits)
 - [ ] L'environement de bureau Gnome (3.36.8)
 - [ ] Bootloader Grub (2.04-1ubuntu26.13)

# Tutoriel : Reproduire mon instalation

## Prérequis :
Attention, les instrucions d'installation sont celles que j'ai utilisées pour mon ordinateur, elles ne seront pas forcément 100% compatibles avec tous les ordinateurs.

Pour le contennu téléchargeable, les liens seront en bas du document dans la partie ressources.
Pour commencer, il vous faudra le matériel suivant:


 - [ ] Une clef usb avec l'image d'ubuntu
 - [ ] Un ordinateur


## 1ère étape : Désactiver le sécure boot.

Lors du démarrage de votre ordinateur, une fonction appelée secure boot permet d'eviter le piratage physique de votre machine. Il faut donc le désactiver pour pouvoir installer un autre système d'exploitation.

Pour commencer, il faut prendre votre ordinateur puis accéder au BIOS, pour cela, il vous suffit d'appuyer sur la touche F10 (ici pour les ordinateurs HP) juste après avoir allumé la machine.

Ensuite vous arriverez dans un menu semblable à celui-ci :

![Bios](https://i.imgur.com/YcMQ6BZ.png)

Il vous suffira de vous rendre dans la rubrique "Security" puis "Secure boot configuration" et enfin décocher la case "secure boot" et pour terminer, il faut enregistrer les modifications puis redémarrer l'ordinateur.


## 2nde étape : Installer Ubuntu

- Branchez la clef USB avec l'image d'Ubuntu puis allumez l'ordinateur en appuyant sur la touche F9 (pour les pc HP) pour accéder au boot menu.

- une fois arrivé sur le boot menu, sélectionnez votre clef usb. L'installation d'Ubuntu va alors débuter.

- Une fois arrivé sur l'ecran suivant, il suffit de suivre les instructions :

![installation ubuntu](https://i.imgur.com/4cO8KnY.jpeg)


Ensuite, vous allez arriver sur une page semblable à celle-ci :

Pour installer le dual boot (choix du système d'exploitation au démarrage), il faut prendre la première option.

![Dual Boot](https://i.imgur.com/LUEcDwK.jpeg)


Puis enfin, il n'y a plus qu'à suivre les instructions. 

Voilà, le Système d'exploitation est installé !


## 3ème etape : Personalisation de Grub

J'ai personalisé mon boot menu GRUB, car c'est toujours plus agréable de passer par une interface "user-friendly".
Pour ce faire, j'ai uttilisé le logiciel "Grub Customizer", celui-ci m'a permis de régler le système d'exploitation par défeaut, le temps de boot automatique par défaut et aussi l'interface graphique.
J'ai donc trouvé un thème sur internet (lien en bas) que j'ai personalisé avec mon logo.
Après avoir fait les modifications avec "Grub Customizer", les modifications prennent effet dès le prochain redémarage.

Dans mon cas, le résultat est le suivant:

![Grup Perso](https://i.imgur.com/8wXAh4i.jpeg)


## 4ème etape : installation des logiciels requis pour commencer à développer

Voici la liste des logiciels de développement que j'ai installé, et la procédure pour chacun d'eux.

#### 1 : VsCode

Pour installer VsCode, il y a besoin d'aller télécharger l'installateur sur leur site internet.
Une fois téléchargé, il vous suffit d'aller dans un terminal, de vous placer dans le même dossier où vous avez télechargé le fichier et d'entrer la commande suivante ```sudo dpkg -i code_*.deb```
Et voilà, VsCode s'installera.

#### 2 : Java

Pour installer java, il suffit de faire la commande suivante : ```sudo apt install openjdk-11-jdk```
Java est donc maintenant installé.


#### 3 : Docker

Pour installer Docker, il faut faire la commande suivante : ```sudo snap install docker```
Docker est maintenant installé.


### 4 : Barrier

Pour en terminer, j'ai télechargé un petit logiciel s'appelant "barrier", il permet d'utiliser un clavaier et une souris pour deux ordinateurs, celui-ci me permet de poser mon ordinateur portable sur mon bureau et de travailler avec mon ordinateur fixe. Il suffit de glisser la souris comme si c'étais un écran en plus. 
L'avantage de ce logiciel, c'est qu'il est "cross-platform", c'est à dire que mon ordinateur fixe est sous Windows, mon ordinateur portable sous linux, et le lien est parfaitement fait.
De plus, le presse-papier est partagé, ce qui est un réel gain de temps !

je mettrais le lien de ce projet en bas de la page.





#

# Explication de mes choix :

Pour mettre en place cet environnement, nous avions plusieurs solutions proposées : 

- [ ] Utiliser un vieux pc pour installer Linux 
- [ ] Faire un dual boot sur son ordinateur personnel pour avoir le choix entre Linux ou Windows en démarrant le pc
- [ ] Faire une VM (Virtual machine)  
- [ ] Configurer WSL2 sur Windows 


Je devais donc choisir une de ces options. Pour cela, j’en ai essayé plusieurs d’entre-elles : 


#### Pour commencer j’ai testé la virtualisation :

Au début, j’ai trouvé cette solution très avantageuse car cela permettait de continuer à utiliser Windows en même temps que l’on travaille sur Linux. Pour commencer, il a fallu installer un Hyperviseur qui est l’outil qui permet de créer des machines virtuelles. J’avais donc le choix entre VmWare Workstation et Microsoft Hyper-V, mon choix se portait sur une des ces deux solutions car je les avais déjà utilisés dans mon passé. Chacun ont leurs avantages et leurs inconvénients. selene


##### VMWare Workstation :

Pour commencer, je vais parler de VmWare Workstation. Cet hyperviseur a retenu mon attention tout simplement pour sa facilité d’utilisation. J’ai donc crée ma machine virtuelle avec les ressoures nécessaire : un processeur avec 2 cœurs virtuels, 6 Go de mémoire, et un disque virtuel de 64 Go. Cette machine était vraiment facile à utiliser, mais plus lente à utiliser qu’une machine physique.


##### Types d'hyperviseurs :

Je me suis donc renseigné sur internet pour savoir pourquoi la machine était si lente, et j’ai trouvé une information très intéressante. Lorsque l’on crée une machine virtuelle sur un pc, on utilise un hyperviseur, comme je l’ai expliqué au dessus. Cet hyperviseur sert à créer virtuellement tous les composants nécessaires au fonctionnement du système d’exploitation (processeur, ram, stockage, …). Il y a donc plusieurs types d’hyperviseurs  : les hyperviseurs de type1 et les hyperviseurs de type2.  VmWare Workstation est un Hyperviseur de type 2, cela veut dire que les informations de la machine virtuelle passent par l’hyperviseur, puis par le système d’exploitation (ici Windows) pour enfin arriver au processeur. C’est parce que les informations sont obligées de repasser par Windows que la machine virtuelle n’était pas assez rapide. La solution est donc d’utiliser un hyperviseur de type1 comme Microsoft Hyper-V. L’avantage d’un hyperviseur de type 1 est de pouvoir exécuter la machine virtuelle sans repasser par Windows, ce qui enlève cette perte de temps.


##### Hyper-V :

 Pour l’installer il suffit d’activer la fonction sur Windows et de simplement redémarrer le pc. Ensuite j’ai créé une machine virtuelle avec les mêmes caractéristiques que la précédente. Le résultat était médiocre. L’interface est moins conviviale, la connexion à la machine virtuelle n'était pas très rapide et je remarquais une petite latence entre les mouvement de souris et ce qui se passait sur l’écran. Ce qui n’est pas très agréable pour travailler.  


#### Seconde solution : WSL-2

Ensuite, par curiosité, j’ai voulu tenter d’installer WSL2 sur Windows, pour cela rien de compliqué. Un petite recherche sur internet suffit pour trouver une commande à entrer dans un terminal powershell : 
``` wsl --install -d Ubuntu ```
après quelques minutes de téléchargement, un simple redémarrage de l'ordinateur suffit. Un nouvel icone est apparu dans le menu démarrer. Il suffit de cliquer dessus pour avoir la console ubuntu. Une fois dedans, tout foncionne comme dans un ordinateur normal. Seul bémol, il n'y a pas d'interface graphique ce qui n'est pas très agréable car entre les navigateurs internet et les editeurs de code, il faudra faire des aller-retours sur windows. Ce n'est pas très convivial.


#### Choix du système d'exploitation :
Lors de mes tests, j'en ai profité pour tester la distribution Ubuntu. Celle-ci m'a beaucoup plu, et j'ai préféré installer celle-ci au lieu de Xubuntu.



---------------------------------------------------------

# Ressources

- Vidéo de démonstration de l'environement : 
    - [Présentation environnemnt de travail SAE Système](https://youtu.be/PFx8grJ0tlE)
---
- Vidéo de demonstration du boot GRUB :
    - [Boot personalisé GRUB](https://youtu.be/qiljra5rA3s)
---
- Téléchargement d'Ubuntu :
    - [Ubuntu Desktop](https://ubuntu.com/download)
---
- Téléchargement de VsCode :
    - [VsCode studio](https://code.visualstudio.com/Download)
---
- Projet Barrier :
    - [Barrier](https://github.com/debauchee/barrier)
---
- Thèmes GRUB :
    - [gnome-looks](https://www.gnome-look.org/browse?cat=109&ord=latest)